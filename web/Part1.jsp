
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <body>
    <table border="1">
      <tr>
        <td></td>
        <% for (int i = 1; i <= 9; i++){ %>
        <td><%=i %></td>
        <%} %>
      </tr>

      <% for (int i = 1; i <= 9; i++){ %>
        <tr>
          <td><%=i %></td>
          <% for (int j = 1; j <= 9; j++){ %>
          <td><%=i * j %></td>
          <%} %>
        </tr>
      <%} %>
    </table>
  </body>
</html>
