<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<body>
<table border="1">
    <tr>
        <td></td>
        <c:forEach begin="1" end="9" var="i">
            <td>${i}</td>
        </c:forEach>
    </tr>
    <c:forEach begin="1" end="9" var="i">
        <tr>
            <td>${i}</td>

            <c:forEach begin="1" end="9" var="j">
                <td>${i*j}</td>
            </c:forEach>
        </tr>
    </c:forEach>
</table>
</body>
</html>