package ua.nure.yaritenko.Practice10;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/Part3")
public class Part3 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().invalidate();
        req.getRequestDispatcher("Part3.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<String> ar;
        req.setCharacterEncoding("UTF-8");
        resp.setContentType("text/html; charset=UTF-8");
        if(req.getSession().getAttribute("list") == null){
            ar = new ArrayList <>();
            ar.add(req.getParameter("name"));
            req.getSession().setAttribute("list", ar);
        }
        else{
            ar = (List<String>)req.getSession().getAttribute("list");
            ar.add(req.getParameter("name"));
            req.getSession().setAttribute("list", ar);
        }
        resp.sendRedirect("Part3.jsp");
    }
}
